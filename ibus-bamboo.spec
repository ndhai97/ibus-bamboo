%define engine_name bamboo
%define ibus_dir     /usr/share/ibus
%define engine_share_dir   /usr/share/ibus-%{engine_name}
%define engine_lib_dir   /usr/lib/ibus-%{engine_name}
%define ibus_comp_dir /usr/share/ibus/component
%define _unpackaged_files_terminate_build 0

Name: ibus-bamboo
Version: v0.8.3
Release: 11
Summary: A Vietnamese input method for IBus

License: GPLv3+
URL: https://gitlab.com/ndhai97/ibus-bamboo
Source0: https://gitlab.com/ndhai97/ibus-bamboo/-/archive/%{version}-RC%{release}/%{name}-%{version}-RC%{release}.tar.gz

BuildRequires: go, ibus-devel, libX11-devel, libXtst-devel, gtk3-devel
Requires: ibus, gtk3

%description
A Vietnamese IME for IBus using Bamboo Engine.
Bộ gõ tiếng Việt mã nguồn mở hỗ trợ hầu hết các bảng mã thông dụng, các kiểu gõ tiếng Việt phổ biến, bỏ dấu thông minh, kiểm tra chính tả, gõ tắt,...

%global debug_package %{nil}
%prep
%autosetup -n %{name}-%{version}-RC%{release}

%build
make build

%install
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root)
%doc README.md
%license LICENSE
%dir %{ibus_dir}
%dir %{ibus_comp_dir}
%dir %{engine_share_dir}
%dir %{engine_lib_dir}
%{engine_share_dir}/*
%{engine_lib_dir}/*
%{ibus_comp_dir}/%{engine_name}.xml

%clean
cd ..
rm -rf %{name}-%{version}-RC%{release}
rm -rf %{buildroot}

%changelog
* Tue Mar 14 2024 NguyenDangHai <nguyenhai97.ict@gmail.com> 0.8.3
- Rebase upstream

* Mon Feb 7 2022 NguyenDangHai <nguyenhai97.ict@gmail.com> 0.8.0
- Init copr support
- support auto rebuild using gitlab ci

* Wed Aug 14 2019 LuongThanhLam <ltlam93@gmail.com> 0.5.3
- Initial RPM release
